# Janky and presently very threadbare test suite for checkem

# This won't pass on operating systems or filesystems that don't support
# symbolic and hard links.

# Create a temporary directory for the tests
td=
td=$(mktemp -d) || exit
out=$td/out err=$td/err
touch -- "$out" "$err" || exit

# Create a subdir for test input files
mkdir -- "$td"/t

# Start counting failures
f=0

# 1: Errors out when run with no arguments
./checkem >"$out" 2>"$err"
ret=$?
case $ret in
    2) ;;
    *)
        printf >&2 '1a: Expected exit value of 2, got %d\n' "$ret"
        f=$((f+1))
        ;;
esac
if [ -s "$out" ] ; then
    printf >&2 '1b: Unexpected stdout (%u bytes):\n' "$(wc -c "$out")"
    cat -- "$out"
    f=$((f+1))
fi
if ! [ -s "$err" ] ; then
    printf >&2 '1c: Expected message on stderr, got none\n'
    f=$((f+1))
fi

# 2: Ignores two different files of different size
printf 'foo\n' > "$td"/t/a
printf 'barbar\n' > "$td"/t/b
./checkem "$td"/t >"$out" 2>"$err"
ret=$?
case $ret in
    0) ;;
    *)
        printf >&2 '2a: Expected exit value of 0, got %d\n' "$ret"
        f=$((f+1))
        ;;
esac
if [ -s "$out" ] ; then
    printf >&2 '2b: Unexpected stdout (%u bytes):\n' "$(wc -c "$out")"
    cat -- "$out"
    f=$((f+1))
fi
if [ -s "$err" ] ; then
    printf >&2 '2c: Unexpected stderr (%u bytes):\n' "$(wc -c "$err")"
    cat -- "$err"
    f=$((f+1))
fi
rm -- "${td:?}"/t/*

# 3: Detects two identical files
printf 'foo\n' > "$td"/t/a
printf 'foo\n' > "$td"/t/b
./checkem "$td"/t >"$out" 2>"$err"
ret=$?
case $ret in
    0) ;;
    *)
        printf >&2 '3a: Expected exit value of 0, got %d\n' "$ret"
        f=$((f+1))
        ;;
esac
if ! [ -s "$out" ] ; then
    printf >&2 '3b: Empty stdout where output expected'
    f=$((f+1))
fi
outl=$(sed '$=;d' "$out")
if [ "$outl" -ne 3 ] ; then
    printf >&2 '3c: Expected 3 lines of output, got %u\n' "$outl"
    f=$((f+1))
fi
if [ -s "$err" ] ; then
    printf >&2 '3d: Unexpected stderr (%u bytes):\n' "$(wc -c "$err")"
    cat -- "$err"
    f=$((f+1))
fi
rm -- "${td:?}"/t/*

# 4: Skips symbolic links
printf 'foo\n' > "$td"/t/a
ln -s -- "$td"/t/a "$td"/t/b
./checkem "$td"/t >"$out" 2>"$err"
ret=$?
case $ret in
    0) ;;
    *)
        printf >&2 '4a: Expected exit value of 0, got %d\n' "$ret"
        f=$((f+1))
        ;;
esac
if [ -s "$out" ] ; then
    printf >&2 '4b: Unexpected stdout (%u bytes):\n' "$(wc -c "$out")"
    cat -- "$out"
    f=$((f+1))
fi
if [ -s "$err" ] ; then
    printf >&2 '4c: Unexpected stderr (%u bytes):\n' "$(wc -c "$err")"
    cat -- "$err"
    f=$((f+1))
fi
rm -- "${td:?}"/t/*

# 5: Skips hard links
printf 'foo\n' > "$td"/t/a
ln -- "$td"/t/a "$td"/t/b
./checkem "$td"/t >"$out" 2>"$err"
ret=$?
case $ret in
    0) ;;
    *)
        printf >&2 '5a: Expected exit value of 0, got %d\n' "$ret"
        f=$((f+1))
        ;;
esac
if [ -s "$out" ] ; then
    printf >&2 '5b: Unexpected stdout (%u bytes):\n' "$(wc -c "$out")"
    cat -- "$out"
    f=$((f+1))
fi
if [ -s "$err" ] ; then
    printf >&2 '5c: Unexpected stderr (%u bytes):\n' "$(wc -c "$err")"
    cat -- "$err"
    f=$((f+1))
fi
rm -- "${td:?}"/t/*

# 6: Ignores two different files of the same size
printf 'foo\n' > "$td"/t/a
printf 'bar\n' > "$td"/t/b
./checkem "$td"/t >"$out" 6>"$err"
ret=$?
case $ret in
    0) ;;
    *)
        printf >&2 '6a: Expected exit value of 0, got %d\n' "$ret"
        f=$((f+1))
        ;;
esac
if [ -s "$out" ] ; then
    printf >&2 '6b: Unexpected stdout (%u bytes):\n' "$(wc -c "$out")"
    cat -- "$out"
    f=$((f+1))
fi
if [ -s "$err" ] ; then
    printf >&2 '6c: Unexpected stderr (%u bytes):\n' "$(wc -c "$err")"
    cat -- "$err"
    f=$((f+1))
fi
rm -- "${td:?}"/t/*

# 7: Detects two identical files in different specified directories
mkdir -- "$td"/t/a "$td"/t/b
printf 'foo\n' > "$td"/t/a/a
printf 'foo\n' > "$td"/t/b/b
./checkem "$td"/t/[ab] >"$out" 2>"$err"
ret=$?
case $ret in
    0) ;;
    *)
        printf >&2 '7a: Expected exit value of 0, got %d\n' "$ret"
        f=$((f+1))
        ;;
esac
if ! [ -s "$out" ] ; then
    printf >&2 '7b: Empty stdout where output expected'
    f=$((f+1))
fi
outl=$(sed '$=;d' "$out")
if [ "$outl" -ne 3 ] ; then
    printf >&2 '7c: Expected 3 lines of output, got %u\n' "$outl"
    f=$((f+1))
fi
if [ -s "$err" ] ; then
    printf >&2 '7d: Unexpected stderr (%u bytes):\n' "$(wc -c "$err")"
    cat -- "$err"
    f=$((f+1))
fi
rm -- "${td:?}"/t/[ab]/*
rmdir -- "${td:?}"/t/[ab]

# Clean up
rmdir -- "${td:?}"/t
rm -- "${td:?}"/*
rmdir -- "$td"

# Report
if [ "$f" -gt 0 ] ; then
    printf >&2 '%u tests failed\n' "$f"
    exit 1
fi
