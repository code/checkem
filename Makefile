.POSIX:
.SUFFIXES:
.PHONY: all install test clean
PREFIX = /usr/local
all:
install:
	mkdir -p -- $(PREFIX)/bin
	cp -- checkem $(PREFIX)/bin
test:
	sh test.sh
clean:
